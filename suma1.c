#include <stdio.h>

int main()
{
	signed long long numero1;  //signed es para que respete los numeros con signo (+ o -)
	signed long long numero2;  //long long es el tipo de dato que se va a utilizar
	signed long long resultado;  
	
	printf(".....SUMA DE NUMEROS.....\n");
	printf("Ingresa un numero: "); //peticiones del numero al usuario
	scanf("%llu",&numero1);  //%llu es la forma en como se lee el tipo de dato signed long long
	printf("Ingresa un numero: ");
	scanf("%llu",&numero2);
		
	if(numero1>0 && numero2>0)  //comparacion para saber si el numero ingresado es positivo o negativp
	{	
		printf("Los numeros son positivos.....\n");
		printf("EL primer numero es: [%llu]\n",numero1); //mostrar cuales fueron los numeros que se ingresaron anteriormente
        printf("EL segundo numero es: [%llu]\n\n",numero2);
		resultado=numero1+numero2;  //funcion para sumar los dos numeros que se ingresaron
        printf("Suma de ");
        printf("[%llu]",numero1);printf("+");printf("[%llu]\n",numero2);
        printf("Da como resultado : %llu\n",resultado); //mostrar lo que hay en la variable de resultado la cual es la suma de los 2 numeros
	}
	else
	{
		printf("ERROR YA QUE UN NUMERO ES NEGATIVO"); //en caso de que un numero de los dos ingresados sea negativo mandara este mensaje de error
	}
	
	return 0;
	
}